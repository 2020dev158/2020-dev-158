# Bowling
===


A kata done for BNP Paribas


Original kata found [here](https://github.com/stephane-genicot/katas/blob/master/Bowling.md)

***

To run the project
---
1. Clone the repository
2. Open the project file (Bowling.xcodeproj) in Xcode
3. Build and run the project on a device or simulator
