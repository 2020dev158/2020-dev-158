import XCTest
@testable import Bowling

class FrameFactoryTests: XCTestCase {

	private func compare(_ rolls: [Int], with frames: [Frame]) {
		var rollIndex = 0
		for (index, frame) in frames.enumerated() {
			XCTAssertEqual(frame.first, rolls[rollIndex])
			XCTAssertEqual(frame.second, rolls[rollIndex+1])

			rollIndex += 2

			if index == frames.count-1 { // Last frame
				if frame.isSpare() {
					XCTAssertEqual(frame.firstBonus, rolls[rollIndex])
				} else if frame.isStrike() {
					XCTAssertEqual(frame.firstBonus, rolls[rollIndex])
					XCTAssertEqual(frame.secondBonus, rolls[rollIndex+1])
				}
			}
		}
	}

	func test20RollsWithNoSpareAndNoStrike() {
		let rolls = [
			3, 5,
			6, 2,
			2, 5,
			6, 1,
			2, 4,
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			9, 0
		]
		let frames = FrameFactory.frames(from: rolls)
		compare(rolls, with: frames)
	}

	func test20RollsWithSpareButNoStrike() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			6, 1,
			2, 8, // Spare
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			9, 0
		]
		let frames = FrameFactory.frames(from: rolls)
		compare(rolls, with: frames)
	}

	func test22RollsWithAllStrikes() {
		let rolls = [
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 10
		]
		let frames = FrameFactory.frames(from: rolls)
		compare(rolls, with: frames)
	}

	func test21RollsWithAllSpares() {
		let rolls = [
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5
		]
		let frames = FrameFactory.frames(from: rolls)
		compare(rolls, with: frames)
	}
}
