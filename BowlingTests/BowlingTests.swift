import XCTest
@testable import Bowling

class BowlingTests: XCTestCase {

	var calculator: Calculator!

    override func setUpWithError() throws {
		calculator = Calculator()
    }

    override func tearDownWithError() throws {
		calculator = nil
    }

	func testIfAllRollsAreGutterRollsThenScoreIs0() {
		let rolls = Array(repeating: 0, count: 20)
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 0)
	}

	func testIfAllRollsAre1ThenScoreIs20() {
		let rolls = Array(repeating: 1, count: 20)
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 20)
	}

	func testIfMockGameNoSpareNoStrikeThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 2,
			2, 5,
			6, 1,
			2, 4,
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			9, 0
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 72)
	}

	func testIfMockGameWithSpareNoStrikeThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			6, 1,
			2, 8, // Spare
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			9, 0
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 86)
	}

	func testIfMockGameWithAllSparesThenScoreIsCorrect() {
		let rolls = [
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5, 5,
			5
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 150)
	}

	func testIfMockGameWithStrikeNoSpareThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			10, 0, // Strike
			2, 5,
			6, 1,
			10, 0, // Strike
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			9, 0
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 92)
	}

	func testIfMockGameWithAllStrikesThenScoreIsCorrect() {
		let rolls = [
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 0,
			10, 10
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 300)
	}

	func testIfMockGameWithOnlyLastStrikeThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 3,
			2, 5,
			6, 1,
			2, 7,
			6, 1,
			2, 3,
			5, 1,
			3, 6,
			10, 0,
			10, 10
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 97)
	}

	func testIfMockGameWithSparesAndStrikesThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			10, 0,
			2, 7,
			6, 1,
			2, 8, // Spare
			10, 0,
			3, 6,
			1, 0
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 111)
	}

	func testIfMockGameWithLastSpareFrameButNoBonusFrameThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			10, 0,
			2, 7,
			6, 1,
			2, 8, // Spare
			10, 0,
			3, 6,
			1, 9 // Spare but no bonus frames available
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 120)
	}

	func testIfMockGameWithLastStrikeFrameButNoBonusFrameThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			10, 0,
			2, 7,
			6, 1,
			2, 8, // Spare
			10, 0,
			3, 6,
			10, 0 // Strike but no bonus frames available
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 120)
	}

	func testIfMockGameWithSecondLastStrikeFrameButNoBonusFrameThenScoreIsCorrect() {
		let rolls = [
			3, 5,
			6, 4, // Spare
			2, 5,
			10, 0,
			2, 7,
			6, 1,
			2, 8, // Spare
			1, 0,
			10, 0, // Second last frame is strike but bonus frame is not available
			10, 0 // Last frame is also a strike
		]
		let score = calculator.score(for: rolls)
		XCTAssertEqual(score, 104)
	}
}
