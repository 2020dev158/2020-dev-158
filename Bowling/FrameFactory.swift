import Foundation

private enum Constants {
	static let MAX_FRAMES = 10
}

class FrameFactory {

	class func frames(from rolls: [Int]) -> [Frame] {
		var frames = [Frame]()
		var rolls = rolls

		makeFirst10Frames(from: &rolls, in: &frames)
		makeBonusFrames(from: &rolls, in: &frames)

		return frames
	}

	private class func makeFirst10Frames(from rolls: inout [Int], in frames: inout [Frame]) {
		while rolls.count > 1, frames.count < Constants.MAX_FRAMES {
			let frame = Frame(first: rolls[0], second: rolls[1])
			frames.append(frame)

			rolls.remove(at: 0)
			rolls.remove(at: 0)
		}
	}

	private class func makeBonusFrames(from rolls: inout [Int], in frames: inout [Frame]) {
		guard rolls.count > 0, var lastFrame = frames.last else {
			return
		}

		// At least one bonus roll present, could be for spare or strike
		lastFrame.firstBonus = rolls[0]
		rolls.remove(at: 0)

		updateSecondBonusIfLastFrameIsStrike(&lastFrame, in: &rolls)

		frames[frames.count-1] = lastFrame
	}

	private class func updateSecondBonusIfLastFrameIsStrike(_ lastFrame: inout Frame, in rolls: inout [Int]) {
		guard lastFrame.isStrike() else {
			return
		}
		lastFrame.secondBonus = rolls[0]
		rolls.remove(at: 0)
	}
}
