import UIKit

class ViewController: UIViewController {

	@IBOutlet var rolltextFields: [UITextField]!
	@IBOutlet var bonusRollstextFields: [UITextField]!

	@IBAction func calculateButtonTapped(_ sender: UIButton) {
		let rolls = makeRollsFromTextFields()
		let score = Calculator().score(for: rolls)
		display(score)
	}

	private func makeRollsFromTextFields() -> [Int] {
		(rolltextFields + bonusRollstextFields).map { Int($0.text ?? "0") ?? 0 }
	}

	private func display(_ score: Int) {
		let alert = UIAlertController(title: "Score", message: "\(score)", preferredStyle: .alert)
		alert.addAction(.init(title: "OK", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
}
