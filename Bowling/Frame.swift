import Foundation

private enum Constants {
	static let MAX_POINTS_PER_FRAME = 10
}

struct Frame {
	let first: Int
	let second: Int
	var firstBonus: Int?
	var secondBonus: Int?

	func isSpare() -> Bool {
		first != Constants.MAX_POINTS_PER_FRAME &&
			(first + second) == Constants.MAX_POINTS_PER_FRAME
	}

	func isStrike() -> Bool {
		first == Constants.MAX_POINTS_PER_FRAME
	}
}

