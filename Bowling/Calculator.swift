import Foundation

class Calculator {

	private var frames = [Frame]()

	func score(for rolls: [Int]) -> Int {
		frames = FrameFactory.frames(from: rolls)
		return calculateScore()
	}

	private func calculateScore() -> Int {
		var score = 0
		for (index, frame) in frames.enumerated() {
			score += frame.first + frame.second
			if frame.isSpare() {
				score += addScore(forSpareFrame: frame, at: index)
			} else if frame.isStrike() {
				score += addScore(forStrikeFrame: frame, at: index)
			}
		}
		return score
	}

	private func addScore(forSpareFrame frame: Frame, at index: Int) -> Int {
		var score = 0
		guard isNotLastFrame(index) else {
			// Last frame
			score += frame.firstBonus ?? 0
			return score
		}

		let nextRoll = frames[index+1].first
		score += nextRoll
		return score
	}

	private func isNotLastFrame(_ index: Int) -> Bool {
		index < frames.count-1
	}

	private func addScore(forStrikeFrame frame: Frame, at index: Int) -> Int {
		var score = 0
		guard isNotLastFrame(index) else {
			// Last frame
			score += (frame.firstBonus ?? 0) + (frame.secondBonus ?? 0)
			return score
		}
		score += addScore(forNextFrameAt: index+1)
		return score
	}

	private func addScore(forNextFrameAt index: Int) -> Int {
		let nextFrame = frames[index]
		var score = nextFrame.first
		guard nextFrame.isStrike() else {
			score += nextFrame.second
			return score
		}

		score += addScore(forNextNextFrameAt: index+1)
		return score
	}

	private func addScore(forNextNextFrameAt index: Int) -> Int {
		var score = 0
		if frames.indices.contains(index) {
			let nextNextFrame = frames[index]
			score += nextNextFrame.first
		} else if let lastFrame = frames.last {
			// Second last frame, adding last frame's first bonus
			score += (lastFrame.firstBonus ?? 0)
		}
		return score
	}
}
